import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing-module';
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { HomeView } from './components/views/home/home.view.component';
import { MoviesView } from './components/views/movies/movies.view.component';
import { MovieView } from './components/views/movie/movie.view.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { MoviesComponent } from './components/movies/movies.component';
import { MovieComponent } from './components/movie/movie.component';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeView,
    MoviesView,
    MovieView,
    HomeComponent,
    LoginComponent,
    MoviesComponent,
    MovieComponent,
    MovieDetailComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
