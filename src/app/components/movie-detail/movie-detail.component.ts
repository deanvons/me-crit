import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from 'src/app/models/movie';
import { Review } from 'src/app/models/review';
import { LoginService } from 'src/app/services/loginService';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private readonly http: HttpClient, private readonly movieService:MovieService, private readonly loginService:LoginService) { }

  movieId = 0;
  movie: Movie = {
    movieId: 0,
    title: "none",
    releaseYear: 0,
    director: "none",
    genre: "none",
    picture: "none",
    trailer: "none",
    franchise: 0,
    characters: [
    ]
  }

  rating : Number = 0

  ngOnInit(): void {
    this.movieId = Number(this.route.snapshot.params['movieId'])
    this.http.get(`https://localhost:44359/api/Movies/${this.movieId}`)
      .subscribe({
        next: (result: Movie | any) => { this.movie = result },
        error: (error) => { console.log(error) }
      })
  }

  submitReview(){
    //save review to state
    let review:Review = {
      MovieID:this.movie.movieId,
      ReviewerUsername: this.loginService.user.fullName,
      Rating:this.rating
    }

    this.movieService.addReview(review)
  }



}
