import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map, tap } from 'rxjs';
import { User } from 'src/app/models/user';
import { LoginService } from 'src/app/services/loginService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  username: string = ""
  password: string = ""

  constructor(private readonly http: HttpClient, private readonly router: Router, private readonly loginService: LoginService) { }

  get user(): User {
    return this.loginService.user
  }

  loginAttempt() {
    this.loginService.login(this.username, this.password)
    this.router.navigate(['movies'])
      

  }
}
