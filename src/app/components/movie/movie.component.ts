import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Movie } from 'src/app/models/movie';


@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  constructor() { }

  @Input() movie?:Movie
  @Output() movieSelected : EventEmitter<number> = new EventEmitter();

  ngOnInit(): void {
  }

  handleSelected(){
    this.movieSelected.emit(this.movie?.movieId)
  }
}
