import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Movie } from 'src/app/models/movie';
import { LoginService } from 'src/app/services/loginService';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  movies: Movie[] = []

  constructor(private readonly http: HttpClient, private router: Router, private readonly loginService: LoginService, private readonly movieService: MovieService) { }

  ngOnInit(): void {
    this.movieService.loadMovies()
      .subscribe({
        next: (result: Movie[] | any) => { this.movies = result },
        error: (error) => { console.log(error) }
      })

  }

  get user() {
    return this.loginService.user
  }

  critMovie(movieId: number) {
    this.router.navigate(['movie', movieId])
    console.log(movieId)
  }


}
