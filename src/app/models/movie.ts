export interface Movie {
    movieId: number;
    title: string;
    releaseYear: number;
    director: string;
    genre: string;
    picture: string;
    trailer: string;
    franchise: number;
    characters: number[];
}