export interface Review{
    MovieID:Number
    Rating:Number,
    ReviewerUsername:string
}