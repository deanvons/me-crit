

export interface User{
    preferred_username: string,
    fullName: string,
    firstName:string,
    email: string,
    lastName: string
}