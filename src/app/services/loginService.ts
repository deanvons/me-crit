import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { User } from "../models/user";


@Injectable({
    providedIn: "root"
})
export class LoginService {

    private KC_BASE_URL = "https://keycloak-authentication.herokuapp.com"
    private _user: User = {
        preferred_username: "none",
        fullName: "none",
        firstName: "none",
        email: "none@none",
        lastName: "none"
    }


    constructor(private readonly http: HttpClient) { }

    get user(){
        return this._user
    }

    set user(user:User){
        this._user = user
    }

    login(username: string, password: string): void {
        const headers = new HttpHeaders({
            "Content-Type": "application/x-www-form-urlencoded"
        })


        var urlencoded = new URLSearchParams();
        urlencoded.append("client_id", "me-crit");
        urlencoded.append("username", username);
        urlencoded.append("password", password);
        urlencoded.append("grant_type", "password");

        this.http.post<any>(`${this.KC_BASE_URL}/auth/realms/me-crit/protocol/openid-connect/token`, urlencoded, { headers })
        .pipe(
            map(kcResult => {
              return {
                ...kcResult,
                decodedToken: JSON.parse(atob(kcResult.access_token.split('.')[1]))
               }
             })
          )
          .subscribe({
           next: (result)=>{
            localStorage.setItem('authToken', result.access_token)
             this._user = {
                 preferred_username: result.decodedToken.preferred_username,
                 fullName: result.decodedToken.name,
                 firstName: result.decodedToken.given_name,
                 lastName:result.decodedToken.family_name,
                 email:result.decodedToken.email
             }
             console.log(this._user)
           },
           error:(error) => {console.log("hello", error)}
         })
    }
}

