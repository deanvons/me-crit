import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Movie } from '../models/movie';
import { Review } from '../models/review';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private _reviews : Review[] = []

  constructor(private readonly http: HttpClient) { }


  loadMovies(): Observable<any> {

    let auth_token = localStorage.getItem('authToken')

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${auth_token}`
    })
    return this.http.get('https://localhost:44359/api/Movies', { headers })
  }

  get reviews(){
    return this._reviews
}


 addReview(review:Review){
  this._reviews.push(review)
 }



}
