import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeView } from "./components/views/home/home.view.component";
import { MovieView } from "./components/views/movie/movie.view.component";
import { MoviesView } from "./components/views/movies/movies.view.component";



const routes: Routes = [
    {
        path:"",
        pathMatch:"full",
        component:HomeView

    },
    {
        path:"movies",
        component:MoviesView
    }
    ,
    {
        path:"movie/:movieId",
        component:MovieView
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{}